package Mojo::Util::Lottery;
use Mojo::Base -base;

use Exporter 'import';

our @EXPORT_OK = qw(
    lottery
);

our $VERSION = '0.0.1';

has 'chances' => sub { 1 };
has 'outOf' => sub { 1000 };

has 'winner' => sub { sub {} };
has 'loser' => sub { sub {} };

has 'resultFactory' => sub {
    return sub {
        my $chances = shift;
        my $outOf = shift;

        my $random_number = int(rand($outOf)) + 1;

        if ($random_number <= $chances) {
            return 1;
        }

        return 0;
    };
};

sub choose {
    my $self = shift;

    my $wins = $self->wins();

    if ($wins) {
        $self->winner->();
    } else {
        $self->loser->();
    }

    return $wins;
}

sub lottery {
    my $chances = shift;
    my $outOf = shift;

    return Mojo::Util::Lottery->new(
        chances => $chances,
        outOf => $outOf,
    );
}

sub wins {
    my $self = shift;

    return $self->resultFactory->($self->chances, $self->outOf);
}

1;
