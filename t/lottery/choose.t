#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

use Mojo::Util::Lottery qw(lottery);

my $wins = 0;
my $losses = 0;

my $always_winner = lottery(10, 10)
    ->winner(sub {
        $wins++;
    })->loser(sub {
        $losses++;
    })->choose;

is($wins, 1, 'always win');
is($losses, 0, 'always lose');
is($always_winner, 1);

$wins = 0;
$losses = 0;

$always_winner = lottery(0, 10)
    ->winner(sub {
        $wins++;
    })->loser(sub {
        $losses++;
    })->choose;

is($wins, 0, 'never win');
is($losses, 1, 'never lose');
is($always_winner, 0);

done_testing();
