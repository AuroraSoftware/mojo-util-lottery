#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

use Mojo::Util::Lottery qw(lottery);

my $lottery = lottery();

isa_ok($lottery, 'Mojo::Util::Lottery');

done_testing();
