# Mojo::Util::Lottery

This package is going to provide a set of utilities for lottery operations.

# Usage

```perl
use Mojo::Util::Lottery qw(lottery);

my $winner = lottery(10, 100)->choose;
```
